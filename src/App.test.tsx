import React from 'react';
import { render ,screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import App from './App';

beforeEach(()=>{
  render(<App />)
})

test('test increment',()=>{
  const btnElement= screen.getByTestId('inc')
  const heading=screen.getByTestId('value')
  let initValue=Number(heading.innerHTML)
  userEvent.click(btnElement)
  expect(Number(heading.innerHTML)).toEqual(initValue+1)

})

test('decrement',()=>{
  const btnElement= screen.getByTestId('dec')
  const heading=screen.getByTestId('value')
  let initValue=Number(heading.innerHTML)
  userEvent.click(btnElement)
  expect(Number(heading.innerHTML)).toEqual(initValue-1)
  
})