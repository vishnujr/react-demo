import { useState } from 'react';
import './App.css';


function App() {
  const [count,setCount]=useState(0)
  
  return (
    <div className='base'>
      <h1 className='header' data-testid='value'>This is a Counte Application</h1>
  <div className='counter'>
<h1>{count}</h1>
<div className='btn-container'>
<button data-testid='dec' className='btn' onClick={()=>setCount(prev=>prev-1)}>-</button>
<button data-testid='inc' className='btn' onClick={()=>setCount(prev=>prev+1)}>+</button>
</div>
    </div>
    </div>
  
  );
}

export default App;
